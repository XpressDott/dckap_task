<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function list(Request $request)
    {
        $categories = Category::latest()->get();
        //dd($categories);
        $count= 1;
        return view('admin.category.list',compact('categories','count'));
    }

    public function create(Request $request)
    {
        return view('admin.category.create');
    }

    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view('admin.category.edit',compact('category'));
    }

    public function delete($id)
    {
        $category = Category::where('id',$id)->delete();
        return redirect()->route('category.list');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
         ]);

        $data= Category::create([
            'name' => $request->name,
            'status' => 'active',
        ]);

        alert()->success('Success!', 'Created Successfully.');
        return redirect()->route('category.list');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
         ]);

       $data= Category::where('id',$request->id)->update([
            'name' => $request->name,
            'status' => $request->status,
        ]);

        alert()->success('Success!', 'Updated Successfully.');
        return redirect()->route('category.list');
    }

    public function get_category_status(Request $request)
    {
        $category = Category::where('id', $request->id)->first();
        $data = Category::where('id', $request->id)->update([
               'status' => $category->status == 'active' ? 'inactive' : 'active',
           ]);


           $status = Category::where('id', $request->id)->first();

           return response()->json($status);
    }

    public function get_category_view(Request $request)
    {
        $category = Category::where('id', $request->id)->first();
       // dd($category);
      // $artist= $category->artist_detail;

        $html= "<table class='table table-bordered'>
                    <tr>
                        <td><b>Name</b></td>
                        <td>$category->name</td>
                    </tr>
                    <tr>
                        <td><b>Status</b></td>
                        <td><b>$category->status</b></td>
                    </tr>
                </table>";

           return response()->json($html);
    }
}
