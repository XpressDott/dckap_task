<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\User;
use Illuminate\Support\Facades\Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function upload_image($file_name, $folder_name, $image)
    {
        $name = $file_name.'_'.date('m_d_Y_h_i_a.').$image->getClientOriginalExtension();
        $path = '/'.$folder_name.'/';
        \Storage::disk('public')->put($path.$name, file_get_contents($image), 'public');
        return $name;
    }

    public function sendPushNotification($notification, $artists ,$notification_id, $artist)
    {
        # prep the bundle
        $msgJson = array(
            'body' => $notification->title,
            'title'	=> 'web App - Notification',
            'icon'	=> 'myicon',
            'sound' => 'mySound',
            'key' => 'artist_notification',
            'notification_id' => $notification_id,
            'data' => $notification
        );
        $apiAccessKey = Config::get('fcm.android');
            $deviceToken = User::where('id', $artist)->value('device_token');
            if($deviceToken != ''){
                $fields = array('to' => $deviceToken, 'data' => $msgJson);
                $headers = array('Authorization: key=' . $apiAccessKey,'Content-Type: application/json');
                # Send Reponse To FireBase Server
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            }
        return;
    }

}
