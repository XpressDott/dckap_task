<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Category;
use App\Industry;
use App\MotherTongue;
use App\Profession;
use App\Subcategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $category = Category::count();
        $sub_category= Subcategory::count();

        return view('admin.index',compact('category','sub_category'));
    }
}
