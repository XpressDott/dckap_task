<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function list(Request $request)
    {
        $categories = Subcategory::with('category')->latest()->get();
        $count= 1;
        return view('admin.subcategory.list',compact('categories','count'));
    }

    public function create(Request $request)
    {
        $category= Category::where('status','active')->get();
        return view('admin.subcategory.create',compact('category'));
    }

    public function edit($id)
    {
        $category= Category::where('status','active')->get();
        $subcategory = Subcategory::where('id',$id)->first();
        return view('admin.subcategory.edit',compact('subcategory','category'));
    }

    public function delete($id)
    {
        $subcategory = Subcategory::where('id',$id)->delete();
        return redirect()->route('subcategory.list');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required',
         ]);

        $data= Subcategory::create([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'status' => 'active',
        ]);

        alert()->success('Success!', 'Created Successfully.');
        return redirect()->route('subcategory.list');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required',
         ]);

       $data= Subcategory::where('id',$request->id)->update([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'status' => $request->status,
        ]);

        alert()->success('Success!', 'Updated Successfully.');
        return redirect()->route('subcategory.list');
    }

    public function get_subcategory_status(Request $request)
    {
        $subcategory = Subcategory::where('id', $request->id)->first();
        $data = Subcategory::where('id', $request->id)->update([
               'status' => $subcategory->status == 'active' ? 'inactive' : 'active',
           ]);


           $status = Subcategory::where('id', $request->id)->first();

           return response()->json($status);
    }

    public function get_subcategory_view(Request $request)
    {
        $subcategory = Subcategory::where('id', $request->id)->with('category')->first();
       // dd($subcategory);
       $category= $subcategory->category;

        $html= "<table class='table table-bordered'>
                    <tr>
                        <td><b>Category Name</b></td>
                        <td>$category->name</td>
                    </tr>
                    <tr>
                        <td><b>Name</b></td>
                        <td>$subcategory->name</td>
                    </tr>
                    <tr>
                        <td><b>Status</b></td>
                        <td><b>$subcategory->status</b></td>
                    </tr>
                </table>";

           return response()->json($html);
    }
}
