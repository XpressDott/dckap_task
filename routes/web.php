<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('login/google', [LoginController::class, 'redirectToProvider']);
Route::get('login/google/callback', [LoginController::class, 'handleProviderCallback']);


Route::middleware(['auth'])->group(function () {

Route::get('/category/list', 'CategoryController@list')->name('category.list');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category/store', 'CategoryController@store')->name('category.store');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
Route::get('/category/delete/{id}', 'CategoryController@delete')->name('category.delete');
Route::get('/get/category/status/', 'CategoryController@get_category_status')->name('category.get_status');
Route::get('/get/category/view/', 'CategoryController@get_category_view')->name('category.get_view');
Route::post('/category/update', 'CategoryController@update')->name('category.update');


Route::get('/subcategory/list', 'SubcategoryController@list')->name('subcategory.list');
Route::get('/subcategory/create', 'SubcategoryController@create')->name('subcategory.create');
Route::post('/subcategory/store', 'SubcategoryController@store')->name('subcategory.store');
Route::get('/subcategory/edit/{id}', 'SubcategoryController@edit')->name('subcategory.edit');
Route::get('/subcategory/delete/{id}', 'SubcategoryController@delete')->name('subcategory.delete');
Route::get('/get/subcategory/status/', 'SubcategoryController@get_subcategory_status')->name('subcategory.get_status');
Route::get('/get/subcategory/view/', 'SubcategoryController@get_subcategory_view')->name('subcategory.get_view');
Route::post('/subcategory/update', 'SubcategoryController@update')->name('subcategory.update');


});
