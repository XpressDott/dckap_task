<!DOCTYPE html>
<html>
@include('admin.header_script')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('admin.header_bar')
  <!-- /.navbar -->
@include('admin.side_bar')
  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Category Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Create </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{route('category.store')}}" method="post" role="form" id="register">
                @csrf
                  @if ($errors->any())
                    <div class="alert alert-danger">
                          <ul>
                             @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                             @endforeach
                           </ul>
                        </div>
                    @endif
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputname">Title</label>
                      <div class="col-sm-6">
                      <input type="text" name="name" class="form-control"  placeholder="Enter the Title of the Category ">
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('admin.footer')
 <script src="{{asset('js/jquery.validate.js')}}"></script>

 <script>
     $('#register').validate({ // initialize the plugin

         rules: {
            name: {
                 required: true,
             },

         }
     });


</script>
