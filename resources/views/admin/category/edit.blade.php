<!DOCTYPE html>
<html>
@include('admin.header_script')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('admin.header_bar')
  <!-- /.navbar -->
@include('admin.side_bar')
  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Category Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">edit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{route('category.update')}}" method="post" role="form" id="register">
                @csrf
            <input type="hidden" name="id" value="{{$category->id}}">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                          <ul>
                             @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                             @endforeach
                           </ul>
                        </div>
                    @endif
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputname">Title</label>
                      <div class="col-sm-6">
                      <input type="text" name="name" class="form-control" value="{{$category->name}}" placeholder="Enter the Title of the category ">
                      </div>
                    </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Status</label>
                           <div class="col-sm-6">
                               <select name="status" class="form-control" >
                                <option value="active" <?php if($category->status == 'active'){ echo"selected"; }?>>Active</option>
                                <option value="inactive"<?php if($category->status == 'inactive'){ echo"selected"; }?>>Inactive</option>
                               </select>
                          </div>
                      </div>

                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('admin.footer')
 <script src="{{asset('js/jquery.validate.js')}}"></script>

 <script>

 //Date picker
 $('#datepicker').datepicker({
      autoclose: true
    })
     $('#register').validate({ // initialize the plugin

         rules: {
            name: {
                 required: true,
             },

         }
     });


</script>
