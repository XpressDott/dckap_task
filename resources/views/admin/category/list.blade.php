<!DOCTYPE html>
<html>
@include('admin.header_script')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('admin.header_bar')
  <!-- /.navbar -->
@include('admin.side_bar')
  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Category Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <div class="col-sm-2">
                      <a href="{{ route('category.create') }}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Add New Category</a>
                      </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Sl No</th>
                      <th>Title</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                  @foreach ($categories as $category)
                  <tr>
                  <td>{{$count++}}</td>
                  <td>{{$category->name}}</td>
                  <td>@if($category->status == 'active')<div class="btn-group"><button type="button"  data-id="{{$category->id}}" class="btn btn-success">Active</button></div>@else<div class="btn-group"><button type="button"  data-id="{{$category->id}}" class="btn btn-danger">Inactive</button></div>@endif</td>
                  <td>
                    <div class="btn-group">
                      <a href="{{ url('/category/edit/'.$category->id) }}" class="btn btn-default btn-flat"><i class="fas fa-edit"></i></a>
                    <button data-toggle="modal"  data-id="{{$category->id}}" class="btn btn-default btn-flat view"><i class="fas fa-eye"></i></button>
                       <a  onclick="return confirm('Are you sure?')" href="{{ url('/category/delete/'.$category->id) }}" class="btn btn-default btn-flat"><i class="fas fa-trash"></i></a>
                    </div>

                    </td>
                  </tr>
                  @endforeach

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Category View</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('admin.footer')

 <script>
    $(document).ready(function(id){
      $('.status').on("click",function(){
          var id= $(this).attr("data-id");

          $.ajax({
            url:'/get/category/status',
            dataType: 'JSON',
            type:'get',
            cache:true,
            data: {
              id: id
            },
            success:  function (response) {
                var status= response.status;
                $('#status'+id).toggleClass('status');
                if(status == 'inactive'){
                  $('#status'+id).removeClass('btn-success').addClass('btn-danger');
                  $('#status'+id).text('Inactive');
                }else{
                  $('#status'+id).removeClass('btn-danger').addClass('btn-success');
                  $('#status'+id).text('Active');
                }

            },
    });
      });

      $('.view').on("click",function(){
          var id= $(this).attr("data-id");

          $.ajax({
            url:'/get/category/view',
            dataType: 'JSON',
            type:'get',
            cache:true,
            data: {
              id: id
            },
            success:  function (response) {
                $('.modal-body').html(response);

            // Display Modal
            $('#modal-lg').modal('show');
            },
    });
      });


  });
</script>
