<!DOCTYPE html>
<html>
@include('admin.header_script')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('admin.header_bar')
  <!-- /.navbar -->
@include('admin.side_bar')
  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Subcategory Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Update </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{route('subcategory.update')}}" method="post" role="form">
                @csrf
            <input type="hidden" name="id" value="{{$subcategory->id}}">
                    <div class="card-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <div class="col-sm-6">
                        <select name="category_id" class="form-control">
                            <option value="">Please Select the Category</option>
                            @foreach ($category as $cat )
                            <option value="{{$cat->id}}" <?php if($subcategory->category_id == $cat->id){ echo"selected"; }?>>{{$cat->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Name</label>
                        <div class="col-sm-6">
                        <input type="text" name="name" class="form-control" value="{{ $subcategory->name }}" placeholder="Enter the Sub Category Name">
                        </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                               <div class="col-sm-6">
                                   <select name="status" class="form-control" >
                                    <option value="active" <?php if($subcategory->status == 'active'){ echo"selected"; }?>>Active</option>
                                    <option value="inactive"<?php if($subcategory->status == 'inactive'){ echo"selected"; }?>>Inactive</option>
                                   </select>
                              </div>
                          </div>
                    </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('admin.footer')

